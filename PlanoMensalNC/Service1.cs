﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace PlanoMensalNC
{
    public partial class Service1 : ServiceBase
    {
        StreamWriter arquivoLog;

        //definição da thread
        Thread _ThreadVerificacao;

        public Service1()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            //arquivoLog = new StreamWriter(@"C:\\logPlanoMensal.txt", true);
            //arquivoLog.WriteLine("Serviço iniciado em: " + DateTime.Now);
            //arquivoLog.Flush();

            //criação da thread de verificação e sua execução
            _ThreadVerificacao = new Thread(VerificarDiaPlano);
            _ThreadVerificacao.Start();
        }

        protected override void OnStop()
        {
            //arquivoLog.WriteLine("Serviço encerrado em : " + DateTime.Now);
            //arquivoLog.Close();
            //paramos a thread quando o serviço for parado
            _ThreadVerificacao.Abort();
        }

        protected void VerificarDiaPlano()
        {
            try
            {
                while (true)
                {
                    int dia = DateTime.Now.Day;
                
                    if ((dia == 5 || dia == 15 || dia == 25) && DateTime.Now.Hour == 5) //Se for dia 5, 15 ou 25
                    {
                        string TokenAcesso = BuscarTokenAsync().Result;

                        //SeuMetodo("Token gerado " + TokenAcesso);

                        var resultGet = MetodoHTTP.GetAsync("compraaplicativo/RealizarCompraPlano", "application/json", TokenAcesso);

                        //SeuMetodo(resultGet.Result.Item1 + " ==== " + resultGet.Result.Item2); //Executará seu método
                    }
                    Thread.Sleep(3600000); //3600000 3.600.000 milisegundos equivalem a 1 hora

                    //SeuMetodo("Passou aqui");
                }
            }
            catch(Exception ex)
            {
                //SeuMetodo(ex.Message);
            }
        }

        protected void SeuMetodo(string texto)
        {
            arquivoLog.WriteLine("Log: " + texto + " às " + DateTime.Now);
            arquivoLog.Flush();
        }

        private async Task<string> BuscarTokenAsync()
        {
            var estudante = new
            {
                Login = "09555693463",
                Senha = "EF797C8118F02DFB649607DD5D3F8C7623048C9C063D532CC95C5ED7A898A64F",
                Tipo = 1
            };

            var resultado = await MetodoHTTP.PostAsync(JsonConvert.SerializeObject(estudante), "login", "application/json", "");

            if (resultado.Item1 != "NotFound" && resultado.Item1 != "Erro" && resultado.Item1 != "NoConected" && resultado.Item1 != "Unauthorized")
            {
                Authentication authentication = JsonConvert.DeserializeObject<Authentication>(resultado.Item2);

                return authentication.AcessToken;
            }
            else
                return "";
        }

        private class Authentication
        {
            public bool Authenticated { get; set; }
            public string Created { get; set; }
            public string Expiration { get; set; }
            public string AcessToken { get; set; }
            public string RefreshToken { get; set; }
            public string Message { get; set; }
        }
    }
}
