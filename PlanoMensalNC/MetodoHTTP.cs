﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace PlanoMensalNC
{
    class MetodoHTTP
    {
        //POST
        public static async System.Threading.Tasks.Task<Tuple<string, string>> PostAsync(string Json, string url, string sContentType, string BT)
        {
            using (HttpClient client = new HttpClient())
            {
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", BT);

                client.BaseAddress = new Uri("https://meunatalcard-pd.concisoti.app:2908/api/");

                using (HttpResponseMessage resposta = await client.PostAsync(url, new StringContent(Json, Encoding.UTF8, sContentType)))
                {
                    var retorno = await RetornoHttp(resposta);

                    return new Tuple<string, string>(retorno.Item1, retorno.Item2);
                }
            }
        }

        //GET
        public static async System.Threading.Tasks.Task<Tuple<string, string>> GetAsync(string url, string sContentType, string BT)
        {
            using (HttpClient client = new HttpClient())
            {
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", BT);

                client.BaseAddress = new Uri("https://meunatalcard-pd.concisoti.app:2908/api/");

                using (HttpResponseMessage resposta = await client.GetAsync(url))
                {
                    var retorno = await RetornoHttp(resposta);

                    return new Tuple<string, string>(retorno.Item1, retorno.Item2);
                }
            }
        }

        private static async System.Threading.Tasks.Task<Tuple<string, string>> RetornoHttp(HttpResponseMessage resposta)
        {
            switch (resposta.StatusCode)
            {
                case System.Net.HttpStatusCode.OK:
                    return new Tuple<string, string>("Ok", await resposta.Content.ReadAsStringAsync());
                case System.Net.HttpStatusCode.NotFound:
                    return new Tuple<string, string>("NotFound", await resposta.Content.ReadAsStringAsync());
                case System.Net.HttpStatusCode.Unauthorized:
                    return new Tuple<string, string>("Unauthorized", await resposta.Content.ReadAsStringAsync());
                case System.Net.HttpStatusCode.BadRequest:
                    return new Tuple<string, string>("Erro", await resposta.Content.ReadAsStringAsync());
                case System.Net.HttpStatusCode.NoContent:
                    return new Tuple<string, string>("NoContent", await resposta.Content.ReadAsStringAsync());
                case System.Net.HttpStatusCode.InternalServerError:
                    return new Tuple<string, string>("InternalServerError", await resposta.Content.ReadAsStringAsync());
                default:
                    return new Tuple<string, string>("TimeOut", await resposta.Content.ReadAsStringAsync());
            }
        }
    }
}
